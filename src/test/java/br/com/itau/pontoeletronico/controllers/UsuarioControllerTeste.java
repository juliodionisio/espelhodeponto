package br.com.itau.pontoeletronico.controllers;


import br.com.itau.pontoeletronico.enums.TipoBatidaEnum;
import br.com.itau.pontoeletronico.models.BatidaDePonto;
import br.com.itau.pontoeletronico.models.Usuario;
import br.com.itau.pontoeletronico.models.dtos.BatidasDePontoDTO;
import br.com.itau.pontoeletronico.services.BatidaDePontoService;
import br.com.itau.pontoeletronico.services.UsuarioService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jdk8.Jdk8Module;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import org.hamcrest.CoreMatchers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@WebMvcTest
public class UsuarioControllerTeste {


    @MockBean
    private UsuarioService usuarioService;

    @MockBean
    private BatidaDePontoService batidaDePontoService;

    @Autowired
    private MockMvc mockMvc;

    Usuario usuario;

    BatidaDePonto batidaDePontoEntrada;

    BatidaDePonto batidaDePontoSaida;

    BatidasDePontoDTO batidasDePontoDTO;

    List<BatidaDePonto> batidas;
    @BeforeEach
    public void setUp(){
        usuario = new Usuario();
        usuario.setCpf("62462389040");
        usuario.setNome("Cassius Clay");
        usuario.setEmail("cassiuslclay@teste.com");

        batidaDePontoEntrada = new BatidaDePonto();
        batidaDePontoEntrada.setUsuario(usuario);
        batidaDePontoEntrada.setTipoBatida(TipoBatidaEnum.ENTRADA);
        batidaDePontoEntrada.setHora(LocalDateTime.of(LocalDate.of(2020,07, 05),
                LocalTime.of(9, 00, 00)));

        batidaDePontoSaida = new BatidaDePonto();
        batidaDePontoSaida.setUsuario(usuario);
        batidaDePontoSaida.setTipoBatida(TipoBatidaEnum.SAIDA);
        batidaDePontoSaida.setHora(LocalDateTime.of(LocalDate.of(2020,07, 05),
                LocalTime.of(11, 00, 00)));

        batidas = new ArrayList<>();
        batidas.add(batidaDePontoEntrada);
        batidas.add(batidaDePontoSaida);

        batidasDePontoDTO = new BatidasDePontoDTO();
        batidasDePontoDTO.setHorasTrabalhadas(2);
        batidasDePontoDTO.setListaDeBatidas(batidas);
    }


    @Test
    public void testarRegistrarUsuario() throws Exception {
        Mockito.when(usuarioService.cadastrarUsuario(usuario)).then(usuarioObjeto -> {
            usuario.setId(1);
            usuario.setDataDeCadastro(LocalDate.of(2020, 11, 20));
            return usuario;
        });

        ObjectMapper mapper = new ObjectMapper();


        String jsonDeUsuario = mapper.writeValueAsString(usuario);

        ResultActions resultActions = mockMvc.perform(MockMvcRequestBuilders.post("/usuarios")
                .contentType(MediaType.APPLICATION_JSON)
                .content(jsonDeUsuario)
                .characterEncoding("utf-8"))
                .andExpect(MockMvcResultMatchers.status().isCreated());
    }

    @Test
    public void testarExibirTodos() throws Exception{
        Iterable<Usuario> usuarios = Arrays.asList(usuario);

        Mockito.when(usuarioService.listarUsuarios()).thenReturn(usuarios);

        mockMvc.perform(MockMvcRequestBuilders.get("/usuarios")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.jsonPath("$.length()").value(1));
    }

    @Test
    public void testarBuscarPorID() throws Exception{
        Mockito.when(usuarioService.buscarUsuarioPorID(Mockito.anyInt())).then(usuarioObjeto -> {
            usuario.setId(1);
            return usuario;
        });

        mockMvc.perform(MockMvcRequestBuilders.get("/usuarios/{id}",1)
                .contentType(MediaType.APPLICATION_JSON)
                .characterEncoding("utf-8"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").value(1));
    }

    @Test
    public void testarBuscarPorIDInexistente() throws Exception{
        Mockito.when(usuarioService.buscarUsuarioPorID(Mockito.anyInt())).thenThrow(RuntimeException.class);

        mockMvc.perform(MockMvcRequestBuilders.get("/usuarios/{id}",3)
                .contentType(MediaType.APPLICATION_JSON)
                .characterEncoding("utf-8"))
                .andExpect(MockMvcResultMatchers.status().isBadRequest());
    }

    @Test
    public void testarAtualizarUsuario() throws Exception{
        Mockito.when(usuarioService.alterarUsuario(1, usuario)).then(usuarioObjeto -> {
            usuario.setId(1);
            usuario.setDataDeCadastro(LocalDate.of(2020, 11, 20));
            return usuario;
        });

        ObjectMapper mapper = new ObjectMapper();


        String jsonDeUsuario = mapper.writeValueAsString(usuario);

        ResultActions resultActions = mockMvc.perform(MockMvcRequestBuilders.put("/usuarios/{id}", 1)
                .contentType(MediaType.APPLICATION_JSON)
                .content(jsonDeUsuario)
                .characterEncoding("utf-8"))
                .andExpect(MockMvcResultMatchers.status().isNoContent());
    }

    @Test
    public void testarAtualizarUsuarioInexistente() throws Exception{
        Mockito.when(usuarioService.alterarUsuario(Mockito.anyInt(), Mockito.any())).thenThrow(RuntimeException.class);

        ObjectMapper mapper = new ObjectMapper();
        String jsonDeUsuario = mapper.writeValueAsString(usuario);

        ResultActions resultActions = mockMvc.perform(MockMvcRequestBuilders.put("/usuarios/{id}", 1)
                .contentType(MediaType.APPLICATION_JSON)
                .content(jsonDeUsuario)
                .characterEncoding("utf-8"))
                .andExpect(MockMvcResultMatchers.status().isBadRequest());
    }

    @Test
    public void testarBaterPonto() throws Exception{
        Mockito.when(batidaDePontoService.baterPonto(Mockito.anyInt())).then(pontoObjeto -> {
            batidaDePontoEntrada.setId(1);
            return batidaDePontoEntrada;
        });

        ObjectMapper mapper = new ObjectMapper();


        String jsonDePonto = mapper.writeValueAsString(batidaDePontoEntrada);

        ResultActions resultActions = mockMvc
                .perform(MockMvcRequestBuilders.post("/usuarios/{id}/baterPonto", 1)
                .contentType(MediaType.APPLICATION_JSON)
                .content(jsonDePonto)
                .characterEncoding("utf-8"))
                .andExpect(MockMvcResultMatchers.status().isCreated());

    }

    @Test
    public void testarEspelhoDePonto() throws Exception{

        Mockito.when(batidaDePontoService.listarBatidasPorUsuario(Mockito.anyInt())).thenReturn(batidas);
        Mockito.when(batidaDePontoService.calcularHorasTrabalhadas(Mockito.any())).thenReturn(2.0);

        mockMvc.perform(MockMvcRequestBuilders.get("/usuarios/{id}/espelhoDePonto",1)
                .contentType(MediaType.APPLICATION_JSON)
                .characterEncoding("utf-8"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.horasTrabalhadas").value(2));
    }

    @Test
    public void testarEspelhoDePontoInexistente() throws Exception{

        Mockito.when(batidaDePontoService.listarBatidasPorUsuario(Mockito.anyInt())).thenThrow(RuntimeException.class);
        Mockito.when(batidaDePontoService.calcularHorasTrabalhadas(Mockito.any())).thenReturn(2.0);

        mockMvc.perform(MockMvcRequestBuilders.get("/usuarios/{id}/espelhoDePonto",1)
                .contentType(MediaType.APPLICATION_JSON)
                .characterEncoding("utf-8"))
                .andExpect(MockMvcResultMatchers.status().isBadRequest());
    }


}
