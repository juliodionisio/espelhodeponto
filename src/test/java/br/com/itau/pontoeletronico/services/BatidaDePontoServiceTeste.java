package br.com.itau.pontoeletronico.services;

import br.com.itau.pontoeletronico.enums.TipoBatidaEnum;
import br.com.itau.pontoeletronico.models.BatidaDePonto;
import br.com.itau.pontoeletronico.models.Usuario;
import br.com.itau.pontoeletronico.repositories.BatidaDePontoRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@SpringBootTest
public class BatidaDePontoServiceTeste {

    @MockBean
    BatidaDePontoRepository batidaDePontoRepository;

    @MockBean
    UsuarioService usuarioService;



    @Autowired
    private BatidaDePontoService batidaDePontoService;

    Usuario usuario;

    List<BatidaDePonto> batidas;

    BatidaDePonto batida1;
    BatidaDePonto batida2;


    @BeforeEach
    public void setUp(){
        usuario = new Usuario();
        usuario.setCpf("62462389040");
        usuario.setNome("Cassius Clay");
        usuario.setEmail("cassiuslclay@teste.com");
        usuario.setId(1);

        batida1 = new BatidaDePonto();
        batida1.setTipoBatida(TipoBatidaEnum.ENTRADA);
        batida1.setUsuario(usuario);
        batida1.setHora(LocalDateTime.now().minusHours(2));
        batida1.setId(1);

        batida2 = new BatidaDePonto();
        batida2.setTipoBatida(TipoBatidaEnum.SAIDA);
        batida2.setUsuario(usuario);
        batida2.setHora(LocalDateTime.now().minusHours(1));
        batida2.setId(2);

        batidas = new ArrayList<>();
        batidas.add(batida1);

    }

    @Test
    public void testarBaterPontoSAIDA(){
        Mockito.when(usuarioService.buscarUsuarioPorID(Mockito.anyInt())).thenReturn(usuario);
        Mockito.when(batidaDePontoRepository.findAllByUsuario(Mockito.any())).thenReturn(batidas);

        BatidaDePonto batidaDePontoTeste = new BatidaDePonto();
        batidaDePontoTeste.setUsuario(usuario);
        batidaDePontoTeste.setTipoBatida(TipoBatidaEnum.SAIDA);


        Mockito.when(batidaDePontoRepository.save(Mockito.any())).then(batidaDePontoLamb -> {
            // Simula o preenchimento do id como se fosse o banco de dados;
            batidaDePontoTeste.setId(3);
            return batidaDePontoTeste;
        });

        BatidaDePonto batidaDePontoObjeto = batidaDePontoService.baterPonto(1);

        Assertions.assertEquals(TipoBatidaEnum.SAIDA, batidaDePontoTeste.getTipoBatida());
        Assertions.assertEquals(3, batidaDePontoTeste.getId());
        Assertions.assertEquals(usuario, batidaDePontoObjeto.getUsuario());
    }

    @Test
    public void testarBaterPontoENTRADA(){
        batidas.add(batida2);
        Mockito.when(usuarioService.buscarUsuarioPorID(Mockito.anyInt())).thenReturn(usuario);
        Mockito.when(batidaDePontoRepository.findAllByUsuario(Mockito.any())).thenReturn(batidas);

        BatidaDePonto batidaDePontoTeste = new BatidaDePonto();
        batidaDePontoTeste.setUsuario(usuario);
        batidaDePontoTeste.setTipoBatida(TipoBatidaEnum.ENTRADA);


        Mockito.when(batidaDePontoRepository.save(Mockito.any())).then(batidaDePontoLamb -> {
            // Simula o preenchimento do id como se fosse o banco de dados;
            batidaDePontoTeste.setId(3);
            return batidaDePontoTeste;
        });

        BatidaDePonto batidaDePontoObjeto = batidaDePontoService.baterPonto(1);

        Assertions.assertEquals(TipoBatidaEnum.ENTRADA, batidaDePontoTeste.getTipoBatida());
        Assertions.assertEquals(3, batidaDePontoTeste.getId());
        Assertions.assertEquals(usuario, batidaDePontoObjeto.getUsuario());
    }

    @Test
    public void testarCalcularHorasTrabalhadas(){
        batidas.add(batida2);
        double horasTrabalhadas;
        horasTrabalhadas = batidaDePontoService.calcularHorasTrabalhadas(batidas);
        Assertions.assertEquals(1, horasTrabalhadas);


    }

}
