package br.com.itau.pontoeletronico.services;


import br.com.itau.pontoeletronico.models.Usuario;
import br.com.itau.pontoeletronico.repositories.UsuarioRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.time.LocalDate;
import java.util.Arrays;

@SpringBootTest
public class UsuarioServiceTeste {

    @MockBean
    private UsuarioRepository usuarioRepository;

    @Autowired UsuarioService usuarioService;

    Usuario usuario;

    @BeforeEach
    public void setUp(){
        usuario = new Usuario();
        usuario.setCpf("62462389040");
        usuario.setNome("Cassius Clay");
        usuario.setEmail("cassiuslclay@teste.com");
        usuario.setId(1);
    }

    @Test
    public void testarCadastrarUsuario(){
        Usuario usuarioTeste = new Usuario();
        usuarioTeste.setCpf("62462389040");
        usuarioTeste.setNome("Cassius Clay");
        usuarioTeste.setEmail("cassiuslclay@teste.com");

        Mockito.when(usuarioRepository.save(usuarioTeste)).then(usuarioLamb -> {
            usuarioTeste.setId(1);
            return usuarioTeste;
        });

        Usuario usuarioObjeto = usuarioService.cadastrarUsuario(usuarioTeste);

        Assertions.assertEquals(1, usuarioTeste.getId());
        Assertions.assertEquals(usuarioObjeto, usuarioTeste);
    }

    @Test
    public void testarBuscarUsuarioPorID(){
        Mockito.when(usuarioRepository.findById(Mockito.anyInt())).thenReturn(java.util.Optional.ofNullable(usuario));
        Usuario usuarioPorID = usuarioService.buscarUsuarioPorID(1);
        Assertions.assertEquals(usuarioPorID, usuario);
    }

    @Test
    public void testarBuscarUsuarioPorIDInexistente(){
        Mockito.when(usuarioRepository.findById(Mockito.anyInt())).thenReturn(null);
        Assertions.assertThrows(RuntimeException.class, () -> {usuarioService.buscarUsuarioPorID(2);});
    }

    @Test
    public void testarListarUsuarios(){
        Iterable<Usuario> usuariosTeste = Arrays.asList();
        Iterable<Usuario> usuarios = usuarioService.listarUsuarios();
        Mockito.when(usuarioRepository.findAll()).thenReturn(usuariosTeste);

        Assertions.assertEquals(usuarios,usuariosTeste);
    }

    @Test
    public void testarAlterarUsuario(){
        Usuario usuarioTeste = new Usuario();
        usuarioTeste.setCpf("62462389040");
        usuarioTeste.setNome("Cassius Clay");
        usuarioTeste.setEmail("cassiuslclay@teste.com");
        usuarioTeste.setDataDeCadastro(LocalDate.of(2020, 07, 05));
        usuarioTeste.setId(1);

        Mockito.when(usuarioRepository.save(usuarioTeste)).then(usuarioLamb -> {
            return usuarioTeste;
        });

        Mockito.when(usuarioRepository.existsById(1)).then(usuarioLamb -> {
            return true;
        });

        Mockito.when(usuarioRepository.findById(Mockito.anyInt())).thenReturn(java.util.Optional.ofNullable(usuario));


        Usuario usuarioObjeto = usuarioService.alterarUsuario(1, usuarioTeste);

        Assertions.assertEquals(1, usuarioTeste.getId());
        Assertions.assertEquals(usuarioObjeto, usuarioTeste);
    }

    @Test
    public void testarAlterarUsuarioInexistente(){
        Mockito.when(usuarioRepository.existsById(1)).then(usuarioLamb -> {
            return false;
        });
        Assertions.assertThrows(RuntimeException.class, () -> {usuarioService.alterarUsuario(2, usuario);});
    }
}
