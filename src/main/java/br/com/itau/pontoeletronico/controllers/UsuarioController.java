package br.com.itau.pontoeletronico.controllers;


import br.com.itau.pontoeletronico.models.BatidaDePonto;
import br.com.itau.pontoeletronico.models.Usuario;
import br.com.itau.pontoeletronico.models.dtos.BatidasDePontoDTO;
import br.com.itau.pontoeletronico.services.BatidaDePontoService;
import br.com.itau.pontoeletronico.services.UsuarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/usuarios")
public class UsuarioController {

    @Autowired
    private UsuarioService usuarioService;

    @Autowired
    private BatidaDePontoService batidaDePontoService;

    @PostMapping
    public ResponseEntity<Usuario> registrarUsuario(@RequestBody @Valid Usuario usuario){
        Usuario usuarioObjeto = usuarioService.cadastrarUsuario(usuario);
        return ResponseEntity.status(201).body(usuarioObjeto);
    }

    @GetMapping
    public Iterable<Usuario> exibirTodos(){
        Iterable<Usuario> usuarios = usuarioService.listarUsuarios();
        return usuarios;
    }

    @GetMapping("/{id}")
    public Usuario buscarPorID(@PathVariable(name = "id") int id){
        try{
            Usuario usuario = usuarioService.buscarUsuarioPorID(id);
            return usuario;
        }catch (RuntimeException exception){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, exception.getMessage());
        }
    }

    @PutMapping("/{id}")
    public ResponseEntity<Usuario> atualizarUsuario(@PathVariable(name = "id") int id, @RequestBody Usuario usuario){
        try{
            Usuario usuarioObjeto = usuarioService.alterarUsuario(id, usuario);
            return ResponseEntity.status(204).body(usuarioObjeto);
        }catch (RuntimeException exception){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, exception.getMessage());
        }
    }

    @PostMapping("/{id}/baterPonto")
    public ResponseEntity<BatidaDePonto>  baterPonto(@PathVariable(name = "id") int id){
        BatidaDePonto pontoObjeto = batidaDePontoService.baterPonto(id);
        return ResponseEntity.status(201).body(pontoObjeto);
    }

    @GetMapping("/{id}/espelhoDePonto")
    public BatidasDePontoDTO espelhoDePonto(@PathVariable(name = "id") int id){
        try{
            BatidasDePontoDTO batidasDePontoDTO = new BatidasDePontoDTO();
            batidasDePontoDTO.setListaDeBatidas(batidaDePontoService.listarBatidasPorUsuario(id));
            batidasDePontoDTO.setHorasTrabalhadas(batidaDePontoService
                    .calcularHorasTrabalhadas(batidasDePontoDTO.getListaDeBatidas()));
            return batidasDePontoDTO;
        }catch (RuntimeException exception){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, exception.getMessage());
        }
    }


}
