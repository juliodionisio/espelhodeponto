package br.com.itau.pontoeletronico.models;


import br.com.itau.pontoeletronico.enums.TipoBatidaEnum;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Entity
public class BatidaDePonto {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @ManyToOne(cascade = CascadeType.ALL)
    @NotNull(message = "É obrigatório o preenchimento do usuário")
    private Usuario usuario;

    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    private LocalDateTime hora;

    @Enumerated(EnumType.STRING)
    @NotNull(message = "É obrigatório selecionar o tipo de batida de ponto")
    private TipoBatidaEnum tipoBatida;

    public BatidaDePonto() {
        this.hora = LocalDateTime.now();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public LocalDateTime getHora() {
        return hora;
    }

    public void setHora(LocalDateTime hora) {
        this.hora = hora;
    }

    public TipoBatidaEnum getTipoBatida() {
        return tipoBatida;
    }

    public void setTipoBatida(TipoBatidaEnum tipoBatida) {
        this.tipoBatida = tipoBatida;
    }
}
