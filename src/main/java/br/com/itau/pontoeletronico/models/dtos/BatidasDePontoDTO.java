package br.com.itau.pontoeletronico.models.dtos;

import br.com.itau.pontoeletronico.models.BatidaDePonto;

import java.util.List;

public class BatidasDePontoDTO {

    private List<BatidaDePonto> listaDeBatidas;

    private double horasTrabalhadas;

    public BatidasDePontoDTO() {
    }

    public List<BatidaDePonto> getListaDeBatidas() {
        return listaDeBatidas;
    }

    public void setListaDeBatidas(List<BatidaDePonto> listaDeBatidas) {
        this.listaDeBatidas = listaDeBatidas;
    }

    public double getHorasTrabalhadas() {
        return horasTrabalhadas;
    }

    public void setHorasTrabalhadas(double horasTrabalhadas) {
        this.horasTrabalhadas = horasTrabalhadas;
    }
}
