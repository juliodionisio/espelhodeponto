package br.com.itau.pontoeletronico.services;


import br.com.itau.pontoeletronico.enums.TipoBatidaEnum;
import br.com.itau.pontoeletronico.models.BatidaDePonto;
import br.com.itau.pontoeletronico.models.Usuario;
import br.com.itau.pontoeletronico.repositories.BatidaDePontoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.SQLOutput;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

@Service
public class BatidaDePontoService {

    @Autowired
    BatidaDePontoRepository batidaDePontoRepository;

    @Autowired
    UsuarioService usuarioService;

    public BatidaDePonto baterPonto(int idUsuario){
        BatidaDePonto ponto = new BatidaDePonto();
        ponto.setUsuario(usuarioService.buscarUsuarioPorID(idUsuario));
        List<BatidaDePonto> batidas = new ArrayList<>();
        batidas = listarBatidasPorUsuario(idUsuario);

        if (batidas.isEmpty()) ponto.setTipoBatida(TipoBatidaEnum.ENTRADA);
        else{
            if(batidas.get(batidas.size() -1).getTipoBatida() == TipoBatidaEnum.ENTRADA){
                ponto.setTipoBatida(TipoBatidaEnum.SAIDA);
            }else{
                ponto.setTipoBatida(TipoBatidaEnum.ENTRADA);
            }
        }


        BatidaDePonto batidaDePontoObjeto = batidaDePontoRepository.save(ponto);
        return batidaDePontoObjeto;
    }

    public List<BatidaDePonto> listarBatidasPorUsuario(int idUsuario) {
        List<BatidaDePonto> batidas = batidaDePontoRepository
                .findAllByUsuario(usuarioService.buscarUsuarioPorID(idUsuario));
        return batidas;
    }

    public double calcularHorasTrabalhadas(List<BatidaDePonto> batidas){
        double totalDeHoras = 0;
        LocalDateTime entrada = null;
        for(BatidaDePonto batida: batidas){
            if(batida.getTipoBatida()== TipoBatidaEnum.ENTRADA){
                entrada = batida.getHora();
            }
            if(batida.getTipoBatida()== TipoBatidaEnum.SAIDA){
                totalDeHoras = totalDeHoras + entrada.until(batida.getHora(), ChronoUnit.MINUTES);
                System.out.println(entrada.until(batida.getHora(), ChronoUnit.MINUTES));
            }
        }
        return totalDeHoras/60;
    }
}
