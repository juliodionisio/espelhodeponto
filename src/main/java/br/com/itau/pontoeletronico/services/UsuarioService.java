package br.com.itau.pontoeletronico.services;

import br.com.itau.pontoeletronico.models.Usuario;
import br.com.itau.pontoeletronico.repositories.UsuarioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class UsuarioService {

    @Autowired
    UsuarioRepository usuarioRepository;

    public Usuario cadastrarUsuario(Usuario usuario){
        Usuario novoUsuario = usuarioRepository.save(usuario);
        return novoUsuario;
    }

    public Usuario buscarUsuarioPorID(int idUsuario){
        Optional<Usuario> optionalUsuario = usuarioRepository.findById(idUsuario);
        if(optionalUsuario.isPresent()){
            return optionalUsuario.get();
        } throw new RuntimeException("Usuário não encontrado");
    }

    public Iterable<Usuario> listarUsuarios(){
        Iterable<Usuario> listaDeUsuarios = usuarioRepository.findAll();
        return listaDeUsuarios;
    }

    public Usuario alterarUsuario(int id, Usuario usuario){
        if(usuarioRepository.existsById(id)){
            usuario.setId(id);
            usuario.setDataDeCadastro(usuarioRepository.findById(id).get().getDataDeCadastro());
            Usuario usuarioObjeto = usuarioRepository.save(usuario);

            return usuarioObjeto;
        }
        throw new RuntimeException("O usuário não foi encontrado");
    }

}
