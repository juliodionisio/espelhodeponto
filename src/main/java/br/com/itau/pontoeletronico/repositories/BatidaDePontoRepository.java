package br.com.itau.pontoeletronico.repositories;

import br.com.itau.pontoeletronico.models.BatidaDePonto;
import br.com.itau.pontoeletronico.models.Usuario;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface BatidaDePontoRepository extends CrudRepository<BatidaDePonto,Integer> {
    List<BatidaDePonto> findAllByUsuario(Usuario usuario);
}
