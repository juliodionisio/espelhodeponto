EndPoints:

POST /usuarios: Efetua o cadastro de um novo usário.
- Exemplo de chamada:
{
"nome":"Cassius Clay",
"email":"cassiuslclay@teste.com",
"cpf":"04983873058"
}

**********

GET /usuarios: Retorna todos os usuários cadastrados. Não é necessário enviar nenhum parâmetro adicional.

**********

GET /usuarios/{id}: Retorna os dados do usuário com o id informado no path da requisição.

**********

PUT /usuarios/{id}: Atualiza os dados do usuário com o id informado no path da requisição.
- Exemplo de chamada:
{
"nome":"Cassius Clay",
"email":"cassiuslclay@teste.com",
"cpf":"04983873058"
}

**********

POST /usuarios/{id}/baterPonto: Inclui uma marcação de ponto para o usuário com o id informado no path da requisição. A marcação é incluída utilizando o horário atual e reconhece qual o ultimo tipo de marcação, para fazer a marcação de ponto com o tipo correto(entrada ou saida). Não é necessário enviar nenhum conteúdo adicional no corpo da requisição.

**********

*GET /usuarios/{id}/espelhoDePonto: Retorna todas as marcações de ponto realizadas pelo usuário com o id informado no path da requisição. Também retorna a quantidade total de horas trabalhadas.






